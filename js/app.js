var marmutApp = angular.module('starter', ['ionic', 'starter.controllers', 'azure-mobile-service.module'])

    .constant('AzureMobileServiceClient', {
        API_URL : 'https://innpmfokrl.azure-mobile.net/',
        API_KEY : 'UVQPaVPmxcaDreqBRqEcczMqxCpvWo59'
    })

marmutApp.factory('marmutFactory',function($http){
    var factory = [];
    factory.getMarmuts = function(){
        return $http.get("http://180.250.66.130/api/talent.xml");
    }
    return factory;
});

marmutApp.run(function($ionicPlatform) {
    $ionicPlatform.ready(function() {
    if (window.cordova && window.cordova.plugins.Keyboard) {
        cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
    }
        if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
        }
    });
})
    .config(function($stateProvider, $urlRouterProvider) {
        $stateProvider
            .state('app', {
                url: "/app",
                abstract: true,
                templateUrl: "templates/menu.html",
                controller: 'AppCtrl'
            })

            .state('share', {
                url: "/share",
                abstract: true,
                templateUrl: "templates/share.html",
                controller: 'AppCtrl'
            })

            .state('app.search', {
                url: "/search",
                views: {
                    'menuContent': {
                        templateUrl: "templates/search.html"
                    }
                }
            })
            .state('app.browse', {
                url: "/browse",
                views: {
                    'menuContent': {
                        templateUrl: "templates/browse.html"
                    }
                }
            })

            .state('app.home', {
                url: "/home",
                views: {
                    'menuContent': {
                        templateUrl: "templates/home.html",
                        controller: 'AppCtrl'
                    }
                }
            })

            .state('app.profil', {
                url : "/profil/:idMarmutId",
                views : {
                    'menuContent' : {
                        templateUrl: "templates/profil.html",
                        controller: 'marmuts'
                    }

                }

            })


        $urlRouterProvider.otherwise('/app/home');
    });
