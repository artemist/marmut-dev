var pageApp = angular.module('starter.controllers', []);

pageApp.controller('marmuts',function($scope,marmutFactory){

    $scope.marmuts = [];
    loadMarmuts();

    function loadMarmuts(){
        marmutFactory.getMarmuts().success(function(data){
            judulBeritas = x2js.xml_str2json(data);
            console.log(judulBeritas.xml.mydata)
            //courses  = x2js.xml_str2json(data);
            //console.log(courses.books.course);
            $scope.marmuts =judulBeritas.xml.mydata;
        });
    }

    $scope.doRefresh = function() {
        loadMarmuts()
        $scope.$broadcast('scroll.refreshComplete');
    }
});

pageApp.controller('AppCtrl', function($scope, $ionicModal, $timeout) {
    $scope.loginData = {};
    $ionicModal.fromTemplateUrl('templates/login.html', {
        scope: $scope
    }).then(function(modal) {
        $scope.modal = modal;
    });

    $scope.closeLogin = function() {
        $scope.modal.hide();
    };

    $scope.login = function() {
        $scope.modal.show();
    };

    $scope.doLogin = function() {
        console.log('Doing login', $scope.loginData);
        $timeout(function() {
            $scope.closeLogin();
        }, 1000);
    };

    $scope.shareAnywhere = function() {
        $cordovaSocialSharing.share("This is your message", "This is your subject", "www/imagefile.png", "http://blog.nraboy.com");
    }

    $scope.shareViaTwitter = function(message, image, link) {
        $cordovaSocialSharing.canShareVia("twitter", message, image, link).then(function(result) {
            $cordovaSocialSharing.shareViaTwitter(message, image, link);
        }, function(error) {
            alert("Cannot share on Twitter");
        });
    }

    $scope.sendFeedback = function () {
        $cordovaSocialSharing
            .shareViaEmail('Some message', 'Some Subject', 'hany@gmail.com');
    }
})


pageApp.controller('SlideController', function($scope) {

    $scope.myActiveSlide = 1;

})



pageApp.controller("ExampleController", function($scope, $ionicSlideBoxDelegate) {
    $scope.navSlide = function(index) {
        $ionicSlideBoxDelegate.slide(index, 500);
    }
})

pageApp.controller('MyCtrl', function($scope) {
    $scope.shouldShowDelete = false;
    $scope.shouldShowReorder = false;
    $scope.listCanSwipe = true
});

